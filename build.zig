const Builder = @import("std").build.Builder;
const bb = @import("std").build;
const std = @import("std");

pub fn build(b: *Builder) void {
    // b.linkSystemLibrary("c");
    const lib = b.addSharedLibrary("popen", "popen.zig", b.version(0, 0, 1));
    lib.addIncludeDir(".");
    lib.linkLibC();
    // lib.setTarget( .os = .windows, .arch = .x86_64, .abi = .gnu } });
    lib.setTheTarget(bb.Target{ .Cross = bb.CrossTarget{ .os = .windows, .arch = .x86_64, .abi = .gnu } });

    // lib.setTheTarget(.{ .Cross = .{ .os = .windows, .arch = .x86_64, .abi = .gnu } });
    // b.addNativeSystemIncludeDir(".");

    b.default_step.dependOn(&lib.step);
}
